// import React, { useContext, useState, useEffect, createContext } from "react";

// export const CartContext = createContext();

// const CartProvider = ({ children }) => {
//   const [cart, setCart] = useState([]);

//   const addToCart = (product, _id) => {
//     const newItem = { ...product, amount: 1 };
//     // check if the item is already in the cart
//     const cartItem = cart.find((item) => {
//       return item._id === _id;
//     });
//     console.log(cartItem);
//     // if cart item is already in the cart
//     if (cartItem) {
//       const newCart = [...cart].map((item) => {
//         if (item._id === _id) {
//           return { ...item, amount: cartItem.amount + 1 };
//         } else {
//           return item;
//         }
//       });
//       setCart(newCart);
//     } else {
//       setCart([...cart, newItem]);
//     }
//   };

//   // remove from cart

//   const removeFromCart = (_id) => {
//     const newCart = cart.filter((item) => {
//       return item._id !== _id;
//     });
//     setCart(newCart);
//   };

//   console.log(cart);
//   return (
//     <CartContext.Provider value={{ cart, addToCart, removeFromCart }}>
//       {children}
//     </CartContext.Provider>
//   );
// };

// export default CartProvider;

import React from "react";

const CartContext = React.createContext();

// creates a React "context object"
// Context Object contains data that can be passed around to multiple props
// Think of it as a delivery container or an empty gift box

export const CartProvider = CartContext.Provider;
// a provider is what is used to distribute the context object to the components

export default CartContext;
