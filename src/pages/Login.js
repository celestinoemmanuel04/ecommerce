// IMPORTS
import {
  Form,
  Button,
  Col,
  Row,
  Container,
  Modal,
  Alert,
  InputGroup,
} from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";
import { Redirect } from "react-router-dom";
import { Link } from "react-router-dom";

// IMAGES
import loginGif from "../images/login.gif";
import logotype from "../images/logotype.png";
import { Navigate } from "react-router-dom";

export default function Login(props) {
  //console.log(props)
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);

  const { user, setUser } = useContext(UserContext);

  // failed
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  // password visibility
  const [showPw, setShowPw] = useState(true);

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  const retrieveUserDetails = (token) => {
    console.log(token);
    fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        // setUser to these values
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
          email: data.email,
          firstName: data.firstName,
          lastName: data.lastName,
        });
      });
  };

  function loginUser(e) {
    e.preventDefault(); // prevent default form behaviour

    /*
			►fetch request inside this function to allow users to login. 
			►Log in the console the response (access token)
			*/

    fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (typeof data.auth !== "undefined") {
          localStorage.setItem("token", data.auth);
          // call retrieveUserDetails function and pass JWT to it

          retrieveUserDetails(localStorage.getItem("token"));

          //alert (`Welcome back ${email}!`)

          props.history.push("/");
        } else {
          // alert ("Login Failed. Please try again")
          handleShow();
          setEmail("");
          setPassword("");
        }
      });
  }

  return user.id !== null ? (
    <Navigate to="/" />
  ) : (
    <>
      <Container className="mt-5 container-fluid">
        <Row>
          <Col lg={6}>
            <center>
              <img className="img-fluid" src={loginGif} />
            </center>
          </Col>
          <Col lg={6}>
            {show ? (
              <Alert
                variant="danger"
                onClose={() => setShow(false)}
                dismissible
              >
                <Alert.Heading>
                  Oh snap! Your Login details are incorrect.
                </Alert.Heading>
                <p>
                  You must input a valid and registered email and password and
                  try again.
                </p>
              </Alert>
            ) : (
              <></>
            )}
            <Form onSubmit={(e) => loginUser(e)}>
              <h3 id="welcomeText" className="mt-3 text-center">
                Hello There{" "}
              </h3>
              <center>
                <img width="50%" className="img-fluid " src={logotype} />
              </center>

              <Form.Group controlId="userEmail">
                <Form.Label className="mt-3">
                  <strong>Email Address</strong>
                </Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Enter email"
                  onChange={(e) => setEmail(e.target.value)}
                  value={email}
                  required
                />
              </Form.Group>

              <Form.Group controlId="password">
                <Form.Label className="mt-3">
                  <strong>Password</strong>
                </Form.Label>
                {showPw ? (
                  <Form.Control
                    type="password"
                    placeholder="Enter password"
                    onChange={(e) => setPassword(e.target.value)}
                    value={password}
                    required
                  />
                ) : (
                  <Form.Control
                    type="string"
                    placeholder="Enter password"
                    onChange={(e) => setPassword(e.target.value)}
                    value={password}
                    required
                  />
                )}

                <Form.Check
                  type="switch"
                  id="pwswitch"
                  label="Show Password"
                  className="text-muted"
                  onChange={(e) => setShowPw(!showPw)}
                />
              </Form.Group>

              {isActive ? (
                <center>
                  <Button
                    className="mt-3"
                    variant="success"
                    type="submit"
                    id="button"
                  >
                    Log-in
                  </Button>
                </center>
              ) : (
                <center>
                  <Button className="mt-3" variant="dark" id="button" disabled>
                    Log-in
                  </Button>
                </center>
              )}
            </Form>

            <p className="mt-5">
              Don't have an account yet? We got you!{" "}
              <Link className="text-dark" to={"/register"}>
                <strong>Register here</strong>{" "}
              </Link>{" "}
              to start shopping!
            </p>
          </Col>
        </Row>
      </Container>
    </>
  );
}
