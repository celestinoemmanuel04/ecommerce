import { useState, useEffect, useContext } from "react";
import { Container, Card, Button, Alert, Modal, Col } from "react-bootstrap";
import { Link, useParams } from "react-router-dom";
import CartContext from "../CartContext";

import loginGif from "../images/login.gif";
import successCart from "../images/success.png";

export default function SpecificProduct({ match }) {
  //console.log ({match})
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [picture, setPicture] = useState("");

  const [id, setId] = useState("");
  const [qty, setQty] = useState(1);

  // cart setup
  const [cart, setCart] = useState([]);
  const [total, setTotal] = useState(0);

  // failed
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  //added to cart
  const [showAdd, setShowAdd] = useState(false);
  const handleCloseAdd = () => setShowAdd(false);
  const handleShowAdd = () => setShowAdd(true);

  const [cartArr, setCartArr] = useState([]);
  const { badge, setBadge } = useContext(CartContext);
  useEffect(() => {
    // console.log(cartArr);
    if (localStorage.getItem("cart")) {
      setCartArr(JSON.parse(localStorage.getItem("cart")));
      setBadge(cartArr.length);
    }
  }, [cartArr]);

  //FOR GETTING YOUR LOCALSTORAGE CART ITEMS AND SETTING YOUR CART STATE:
  //on component mount, check if there is an existing cart in localStorage. If there is, set its contents as our cart state
  useEffect(() => {
    if (localStorage.getItem("cart")) {
      setCart(JSON.parse(localStorage.getItem("cart")));
    }
  }, []);

  // match.params holds the ID of our product  in the productId
  const { productId } = useParams();
  // console.log(productId);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setPicture(data.image);
        setId(productId);
      });
  }, []);

  function addToCart(e) {
    if (localStorage.getItem("token")) {
      //variable to determine if the item we are adding is already in our cart or not
      let alreadyInCart = false;
      //variable for the item's index in the cart array, if it already exists there
      let productIndex;
      //temporary cart array
      let cart = [];

      if (localStorage.getItem("cart")) {
        cart = JSON.parse(localStorage.getItem("cart"));
      }

      //loop through our cart to check if the item we are adding is already in our cart or not
      for (let i = 0; i < cart.length; i++) {
        if (cart[i].productId === id) {
          //if it is, make alreadyInCart true
          alreadyInCart = true;
          productIndex = i;
        }
      }

      //if a product is already in our cart, just increment its quantity and adjust its subtotal
      if (alreadyInCart) {
        cart[productIndex].quantity += qty;
        cart[productIndex].subtotal =
          cart[productIndex].price * cart[productIndex].quantity;
      } else {
        //else add a new entry in our cart, with values from states that need to be set wherever this function goes
        cart.push({
          productId: id,
          name: name,
          price: price,
          quantity: qty,
          subtotal: price * qty,
          image: picture,
        });
      }

      //set our localStorage cart as well
      localStorage.setItem("cart", JSON.stringify(cart));
      console.log(cart);
      handleShowAdd();
    } else {
      handleShow();
    }
  }

  return (
    <Container className="mt-5">
      {show ? (
        <Alert variant="danger" onClose={() => setShow(false)} dismissible>
          <Alert.Heading>You must be logged in to do that</Alert.Heading>
          <p>
            Login in{" "}
            <Link className="text-dark" to={"/login"}>
              {" "}
              here{" "}
            </Link>{" "}
            to start shopping!
          </p>
        </Alert>
      ) : (
        <></>
      )}

      {showAdd ? (
        <Modal
          className="text-center justify-content-center"
          centered
          show={showAdd}
          onHide={handleCloseAdd}
          animation={true}
        >
          <Modal.Body>
            <strong>
              <p>Successfully added to cart!</p>
            </strong>
            <center>
              <img width="100px" src={successCart} />
            </center>
            <div className=" d-flex justify-content-center align-items-center">
              <Col sm={6}>
                <Link className="nav-link mx-2 mt-3" to="/shop">
                  <Button variant="outline-dark">Back to Shop </Button>
                </Link>

                <Link className="nav-link mx-2 mt-3" to="/cart">
                  <Button variant="outline-dark">Go to Cart </Button>
                </Link>
              </Col>
            </div>
          </Modal.Body>
        </Modal>
      ) : (
        <></>
      )}

      <Card>
        <div class="bg-secondary text-white text-center pb-0 card-header">
          <h4>{name}</h4>
        </div>
        <Card.Body className="text-center">
          <center>
            <img width="200px" src={picture} />
          </center>
          <Card.Title></Card.Title>
          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          <Card.Subtitle>
            Price:
            <span class="text-info"> ₱ {price}</span>
          </Card.Subtitle>
        </Card.Body>
        <Card className="footer p-2">
          <Button
            id="button"
            onClick={(e) => addToCart(e)}
            className="btn btn-success btn-block"
          >
            Add to cart
          </Button>
        </Card>
      </Card>
    </Container>
  );
}
