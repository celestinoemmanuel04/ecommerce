import error from '../images/error.png'


import {Link} from 'react-router-dom'
import {Button, Row, Col, Container, Card} from 'react-bootstrap';

export default function Error ()
{
	return (
		<>
			<Container className= "mt-5 container-fluid">
				<Row>
					<Col>
						<center>
						<img  src={error}/>
						<strong><p>Oops! The page you are trying to access cannot be found.</p></strong>
				
						<Link id="button" className= "btn btn-success" to="/">Back to Home</Link>
						</center>				
					</Col>
				</Row>
			</Container>
		</>

		)
}