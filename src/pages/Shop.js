// IMPORT
//PACKAGES
import { useEffect, useState, useContext } from "react";
import { Col, Card, Row, Container, CardGroup } from "react-bootstrap";

//COMPONENTS
import ProductCard from "../components/ProductCard";
import AdminView from "../components/AdminView";
import UserContext from "../UserContext";

//IMAGES
import header from "../images/header1.png";
import items from "../images/itemslist.png";

// EXPORT
export default function Shop() {
  const [productData, setProductData] = useState([]);

  const { user } = useContext(UserContext);
  //console.log (user)

  //console.log(productData[0]);

  const fetchData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/product/allproducts`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setProductData(data);
      });
  };

  // Fetch by default always makes a GET request, unless a different one is specified

  // ALWAYS add fetch request for getting data in a useEffect hook
  useEffect(() => {
    //console.log(process.env.REACT_APP_API_URL)
    // changes to env files are applied only at build time (when starting the project locally)
    fetchData();
  }, []);

  const products = productData.map((product) => {
    if (product.isAvailable) {
      return <ProductCard productsProp={product} key={product._id} />;
    } else {
      return null;
    }
  });

  return user.isAdmin ? (
    <AdminView productsProp={productData} fetchData={fetchData} />
  ) : (
    <>
      <Container fluid className="justify-content-center py-5">
        <Row>
          <img img-fluid src={header} />
        </Row>
      </Container>

      <h1>
        <center>
          {/* <img width="200px" src={items} /> */}
          <h1> Product List</h1>
        </center>
      </h1>
      <Container fluid>
        <Row>{products}</Row>
      </Container>
    </>
  );
}
