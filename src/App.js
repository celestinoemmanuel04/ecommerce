// IMPORTS

import logo from "./images/logo.png";
import "./App.css";

// COMPONENTS
import AppNavBar from "./components/AppNavBar";
import Footer from "./components/Footer";

// PAGES
import Home from "./pages/Home";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Register from "./pages/Register";
import Shop from "./pages/Shop";
import SpecificProduct from "./pages/SpecificProduct";
import Cart from "./pages/Cart";
import Orders from "./pages/Orders";
import Error from "./pages/Error";
import Profile from "./pages/Profile";

// PACKAGES
import { UserProvider } from "./UserContext";
import { useState, useEffect } from "react";
import { Container } from "react-bootstrap";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Switch,
} from "react-router-dom";
import CartContext, { CartProvider } from "./CartContext";

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
    email: null,
    firstName: null,
    lastName: null,
  });

  const [cartArr, setCartArr] = useState([]);
  const [badge, setBadge] = useState(0);
  useEffect(() => {
    // console.log(cartArr);
    if (localStorage.getItem("cart")) {
      setCartArr(JSON.parse(localStorage.getItem("cart")));
      setBadge(cartArr.length);
    }
  }, [cartArr]);

  useEffect(() => {
    console.log(user);
  }, [user]);

  const unSetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())

      .then((data) => {
        // setUser to these values
        console.log(data);
        if (typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
            email: data.email,
            firstName: data.firstName,
            lastName: data.lastName,
          });
        } else {
          setUser({
            id: null,
            isAdmin: null,
            email: null,
            firstName: null,
            lastName: null,
          });
        }
      });
  }, []);

  return (
    <UserProvider value={{ user, setUser, unSetUser }}>
      <CartProvider value={{ badge, setBadge }}>
        <Router>
          <AppNavBar />

          <Container>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/profile" element={<Profile />} />
              <Route path="login" element={<Login />} />
              <Route path="logout" element={<Logout />} />
              <Route path="register" element={<Register />} />
              <Route path="shop" element={<Shop />} />
              <Route path="/shop/:productId" element={<SpecificProduct />} />
              <Route path="cart" element={<Cart />} />
              <Route path="orders" element={<Orders />} />
              <Route path="*" element={<Error />} />
            </Routes>
          </Container>
        </Router>
        <Footer />
      </CartProvider>
    </UserProvider>
  );
}

export default App;
