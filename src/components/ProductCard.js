import { Col, Card, Row, Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import { Link } from "react-router-dom";

// Destructuring
export default function ProductCard({ productsProp }) {
  //destructuring the products prop
  const { _id, name, description, price, image } = productsProp;

  return (
    <Col xs={12} md={6} lg={3} className="mb-4">
      <Card id="card" className="courseCard my-2 h-100">
        <Card.Body>
          <Row>
            <Col xs={6}>
              <Card.Title>{name}</Card.Title>
            </Col>
            <Col xs={6}>
              <center>
                <img id="prodThumnail" width="100px" src={image} />
              </center>
            </Col>
          </Row>

          <Card.Text className="mt-3">
            <strong>Price: </strong> PhP {price}
          </Card.Text>

          <Card.Subtitle className="mt-3"> Description: </Card.Subtitle>
          <Card.Text>{description}</Card.Text>
        </Card.Body>

        <Card.Footer>
          <center>
            <Link id="button" className="btn btn-primary" to={`/shop/${_id}`}>
              Details
            </Link>
          </center>
        </Card.Footer>
      </Card>
    </Col>
  );
}
