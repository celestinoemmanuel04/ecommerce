import { Col, Card, Row, Button, Accordion } from "react-bootstrap";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Table from "react-bootstrap/ListGroup";

export default function OrderCard({ ordersProp }) {
  console.log(ordersProp);
  //destricturing the products prop
  const { _id, orderStatus, transactionDate, totalAmount, products } =
    ordersProp;

  //console.log(products[0].name)
  console.log(products);

  const listItems = products.map((d) => (
    <>
      {/* <div className="d-flex flex-column justify-content">
        <div>
          Item Name:{" "}
          <span>
            {" "}
            <strong>{d.name}</strong>{" "}
          </span>{" "}
        </div>

        <div>Quantity: {d.quantity} </div>
        <div>Subtotal: ₱{d.subtotal} </div>
      </div> */}

      <tr>
        <td>{d.name}</td>

        <td>{d.quantity}</td>
        <td>{d.price}</td>
        <td> ₱ {d.subtotal}</td>
      </tr>
    </>
  ));

  return (
    <center>
      <Col xs={12} lg={8}>
        <Accordion defaultActiveKey="0">
          <Accordion.Item eventKey={_id}>
            <Accordion.Header>
              ORDER NUMBER: <strong className="d-flex ps-3">{_id}</strong>
            </Accordion.Header>

            <Accordion.Body>
              <div className="align-items-left">
                Total Amount: <strong> ₱ {totalAmount}</strong>
              </div>
              <div className="align-items-left">
                Order Status: <strong>{orderStatus}</strong>
              </div>
              <div className="align-items-left mb-3">
                Placed on:{" "}
                <strong>
                  {transactionDate.slice(0, transactionDate.length - 14)}
                </strong>
              </div>
              <Accordion flush>
                <Accordion.Header>Items:</Accordion.Header>
                <Accordion.Body>
                  <div>
                    <Table striped bordered hover responsive>
                      <thead>
                        <tr>
                          <th>Item name</th>

                          <th className="w-25">Quantity</th>
                          <th className="w-25">Price</th>
                          <th>Subtotal</th>
                        </tr>
                        <tbody>{listItems}</tbody>
                      </thead>
                    </Table>
                  </div>
                </Accordion.Body>
              </Accordion>
            </Accordion.Body>
          </Accordion.Item>
        </Accordion>
      </Col>
    </center>
  );
}
