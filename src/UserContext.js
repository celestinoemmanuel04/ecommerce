// IMPORT
import React from 'react';

const UserContext = React.createContext();
	// creates a React "context object"
	// Context Object contains data that can be passed around to multiple props
	// Think of it as a delivery container or an empty gift box

export const UserProvider = UserContext.Provider;
	// a provider is what is used to distribute the context object to the components

export default UserContext;